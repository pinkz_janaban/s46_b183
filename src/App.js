import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home"

import Courses from "./pages/Courses";
import Home from "./pages/Home";

import {Container} from "react-bootstrap";
import './App.css';

function App() {
  return (

    // In Reactjs, we normally render our components in an entry point or in a mother component called App. This is so we can group our components under a single entry point/main component

    // All other components/pages will be contained in our main component: <App />

    // ReactJS does not like rendering two adjacent elements. instead the adjacent element must be wrapped by a parent element/react fragment
    <>
      <AppNavbar />
      <Container fluid>
        <Home />
        {/*<Home />*/}
        <Courses />
      </Container>
    </>
  );
}

/*
  Mini Activity

  1. Create a CourseCard component showing a particular course with the name, description and price inside a React-Bootstrap Card:
    - The course name should be in the card title.
    - The description and price should be in the card subtitle.
    - The value for description and price should be in the card text.
    - Add a button for Enroll.
  2. Render the CourseCard component in the Home page.
  3. Take a screenshot of your browser and send it in the batch hangouts

*/

export default App;
