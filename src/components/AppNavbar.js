// import Navbar from "react-bootstrap/Navbar";
// import Nav from "react-bootstrap/Nav";

import {Navbar, Nav, Container, NavDropdown} from "react-bootstrap";

export default function AppNavbar(){
	return(
		<Navbar bg="light" expand="lg">
	      <Container>
	        <Navbar.Brand href="#home">Zuitt</Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	    		{/*className is use instead of "class", to specify a CSS classes*/}
	          <Nav className="me-auto">
	            <Nav.Link href="#home">Home</Nav.Link>
	            <Nav.Link href="#courses">Course</Nav.Link>
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	)
}
